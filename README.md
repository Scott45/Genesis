# Genesis
[![forthebadge](https://forthebadge.com/images/badges/check-it-out.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/fuck-it-ship-it.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/just-plain-nasty.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/oooo-kill-em.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com)

## Links
### \[[Discord](https://discord.gg/UMQMg9p)-[Website](http://genesis-game.ga)\]
## Information
Genesis is a codebase inspired by the russian roguelike based on BS12 Luna, Lifeweb. Genesis is based on a more modern Bs12.

Genesis was diverged from BayStation 12 on commit d3bd1c8bbf94b2fbfe6acac30f70d1ce4572582b, on Tuesday, August 7, 2018.

## License
Genesis is, as BayStation, licensed under [aGPLv3](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)).

## How to run
1. Download the code
2. Download BYOND
3. Use DreamMaker to compile source
4. Use DreamDaemon to start hosting.