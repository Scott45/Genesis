# Contribution Guide
## Foreword
**Do not ask for push access**, any and all changes are made thro

## Steps
1. Fork this repo.
2. Make your own branch.
3. Change the branch with the changes you want to make.
4. Add yourself as a dev in `html/changelogs/changelog.yml`
5. Add your changes to `html/changelogs/changelog.yml`

    5a. Please refer to the `html/changelogs/example.yml` for a guide to changelog entry types.
6. On this repo, go to the merge requests tab.
7. Open a merge request.

## Rules
1. Always test your changes.
2. Don't sneak things into the code.
3. Don't break shit intentionally.
4. Remain civil.