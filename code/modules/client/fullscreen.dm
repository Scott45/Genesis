client/var/fullscreen = 0
client/New()
    ..()
    ToggleFullscreen()

client/verb/ToggleFullscreen()
    set name = "Toggle Fullscreen"
    set category = "OOC"
    fullscreen = !fullscreen
    if(fullscreen)
        winset(src, "mainwindow", "is-maximized=false;can-resize=false;titlebar=false;menu=")
        winset(src, "mainwindow.mainvsplit", "splitter=70")
        winset(src, "mainwindow", "is-maximized=true")
    else
        winset(src, "mainwindow", "is-maximized=false;can-resize=true;titlebar=true;menu=menu")
        winset(src, "mainwindow.mainvsplit", "splitter=60")