// Draw interact menu
/mob/living/carbon/human/verb/interact(mob/living/carbon/A as mob in view())
    set name = "Interact"
    set category = "Object"
    if(src == A)
        to_chat(src, "You can't interact with yourself!")
        return
    src.partner = A
    var/menu = ""
    if(A in range(1, src))
        menu += "<h2 style='color: red'>Close Ranged</h2><br>"
        menu += "<a href='?src=\ref[usr];interaction=slap'>Slap</a><br>"
        menu += "<a href='?src=\ref[usr];interaction=handshake'>Shake Hand</a><br>"
        menu += "<a href='?src=\ref[usr];interaction=hug'>Hug</a><br>"
        menu += "<a href='?src=\ref[usr];interaction=lip_kiss'>Kiss Lips</a><br>"
        menu += "<a href='?src=\ref[usr];interaction=cheek_kiss'>Kiss Cheeks</a><br>"
        menu += "<a href='?src=\ref[usr];interaction=fistbump'>Fist Bump</a><br>"
        menu += "<a href='?src=\ref[usr];interaction=highfive'>High Five</a><br>"
    menu += "<h2 style='color: red'>Long Ranged</h2><br>"
    menu += "<a href='?src=\ref[usr];interaction=fingergun'>Fingergun</a><br>"
    menu += "<a href='?src=\ref[usr];interaction=flip_off'>Flip Off</a><br>"
    var/datum/browser/popup = new(usr, "interactions", "Interactions: <i>[A]</i>", 340, 480)
    popup.set_content(menu)
    popup.open()